# [SECTION] Python Class Review

class SampleClass():
		def __init__(self, year):
				self.year = year

		def show_year(self):
			print(f'The year is: {self.year}')

myObj = SampleClass(2020)

print(myObj.year)
myObj.show_year()

#  [SECTION]  Fundamentals of OOP
#	There are four main fundamental principles in OOP
#	Encapsulation
# 	Inheritance
#	Polymorphism
# 	Abstraction

# [SECTION]  Encapsulation
# Encapsulation is a mechanism of wrapping the 
#  attributes and codes acting on the methods
#  together as a single unit.
# "data hiding"

# The prefix underscore(_) is used as a warning
# for developers that means:
# "Please be careful about this attribute or method, 
# don't use it outside the declared Class."

class Person():
	def __init__(self):
		# protected attribute _name
		self._name = "John Doe"
		self._age = 0

	# setter method (modifying)
	def set_name(self, name):
		self._name = _name



	def set_age(self, age):
		self._age = age

	# getter method (viewing)
	def get_name(self):
		print(f'Name of Person: {self._name}')	

p1 = Person()
print(p1._name) #this will return an attribute error
p1.get_name()
p1.set_name("Jane Smith")
p1.get_name()

# Mini Exercise
#  Add another protected attribute called "age" 
#  and create the necessary getter and setter methods

# Test Case
p1.get_age()
p1.set_age(40)
p1.get_age()

# Sample output for get_age(): Age of Person: 40


# [SECTION]  Inheritance
# The transfer of the characteristics pf the parent
# class to child classes are derived from it.
# "Parent-Child Relationship"
#  To create an inherited class, in the className
#  definition with add the parent class as the parameter
# pf the child

class Employee(Person):
	def __init__(self, employeeID):
		# super() can be used to invoke the immediate
		# parent class constructor.
	super().__init__():
	#  unique attribute to the Employee class
	self._employeeId = employeeId

#  Methods of the Employee class
	def get_employeeId(self):
		print(f'The Employee ID is {self._employeeId}')

	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# Details methods
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.get_name()
emp1.get_age()
emp1.set_name("Jane Smith")
emp1.set_name(40)
emp1.get_details()	




# Mini exercise
	# 1. Create a new class called Student that 
	# inherits Person with the additional attributes 
	# and methods
	# attributes: Student No, Course, Year Level

	# methods:
	# 	Create the necessary getters and setters for each 
	# attribute

	# 	get_detail: prints the output "‹Student name› is 
	# currently in year < year level> taking up ‹Course›"


# Sample output for get_detail: John Doe is currently
# in year 4 taking up BSIT.

class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__()
		# attributes unique to Student class
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	# getters
	def get_student_no(self):
		print(f"Student number of Student is {self._student_no}")

	def get_course(self):
		print(f"Course of student is {self.course}")

	def get_year_level(self):
		print()		

















































