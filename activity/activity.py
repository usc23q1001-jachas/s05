from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod

	def eat(self, food):
		pass

	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, Name, Breed, Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

	def get_name(self):
		print(f"Name of cat: {self._Name}")

	def get_breed(self):
		print(f"Breed of cat: {self._Breed}")

	def get_age(self):
		print(f"Age of cat: {self._Age}")

	def set_name(self, Name):
		self._Name = Name

	def set_breed(self, Breed):
		self._Breed = Breed

	def set_age(self, Age):
		self._Age = Age

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Myeeowwrr! Nyaawrr! Narrr!")

	def call(self):
		print(f"{self._Name}, come on!")


class Dog(Animal):
	def __init__(self, Name, Breed, Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

	def get_name(self):
		print(f"Name of dog: {self._Name}")

	def get_breed(self):
		print(f"Breed of dog: {self._Breed}")

	def get_age(self):
		print(f"Age of dog: {self._Age}")

	def set_name(self, Name):
		self._Name = Name

	def set_breed(self, Breed):
		self._Breed = Breed

	def set_age(self, Age):
		self._Age = Age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print(f"WOOF! WOOF!")

	def call(self):
		print(f"Here {self._Name}!")


# Test Cases:
dog1 = Dog("Matcha", "Maltese", 20)
dog1.eat("Chicken")
dog1.make_sound()
dog1.call()
print(" ")
cat1 = Cat("Casper", "Persian", 10)
cat1.eat("Fish")
cat1.make_sound()
cat1.call()